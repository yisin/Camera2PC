package tianwu.pic;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import javax.imageio.ImageIO;

public class NetworkServer extends Thread{
	private ServerSocket ss;
	public NetworkServer(int port){
		try {
			ss = new ServerSocket(port);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public void run(){
		try {
			while(true){
				Socket socket = ss.accept();
				new PictureUI(ImageIO.read(socket.getInputStream()));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
