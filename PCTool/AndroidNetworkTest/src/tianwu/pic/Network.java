package tianwu.pic;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Network extends Thread{

	private ServerSocket ss;
	
	private UI ui;
	
	
	public Network(int port,Object o){
		try {
			ss = new ServerSocket(port);
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.ui = (UI)o;
	}
	public void run(){
		try {
			while(true){
				Socket socket = ss.accept();
				System.out.println("new data.");
				new SocketThread(ui,socket).start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
