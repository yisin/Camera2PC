package com.example.tianwu.camera2pc;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 * Created by Tianwu on 2015/8/17.
 */
public class NetworkPicture extends Thread {
    private String host;
    private int port;
    private byte[] data;

    public NetworkPicture(String host,int port,byte[] data){
        this.host = host;
        this.port = port;
        this.data = data;
    }

    public void run(){
        try{
            Socket socket = new Socket(host,port);
            DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
            dos.write(data);
            dos.flush();
            socket.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
